import {
  Controller,
  Get,
  UseInterceptors,
  UploadedFiles,
  Post,
  Request,
} from '@nestjs/common';
import { DefaultService } from './default.service';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller()
export class DefaultController {
  constructor(private readonly appService: DefaultService) {}

  @Get()
  getHello() {
    return this.appService.getHello();
  }

  @Get('/send')
  async send() {
    await this.appService.sendToFixedConsumer();
    return this.appService.send('test');
  }

  @Post('/upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@Request() req) {
    // console.log(req.file);
    console.log('controoller called received');

    await this.appService.send(req.file);
  }
}
