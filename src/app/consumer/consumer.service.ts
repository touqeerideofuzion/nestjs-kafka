import { Injectable } from '@nestjs/common';
import { AbstractKafkaConsumer } from '../common/kafka/kafka.abstract.consumer';
import {
  SubscribeTo,
  SubscribeToFixedGroup,
} from '../common/kafka/kafka.decorator';
import { KafkaPayload } from '../common/kafka/kafka.message';
import { HELLO_FIXED_TOPIC } from '../constant';

// Load the SDK and UUID
var AWS = require('aws-sdk');
var uuid = require('node-uuid');
var fs = require('fs');
// var fileStream = fs.createReadStream('./test.pdf');
// Create an S3 client
// var credentials = new AWS.SharedIniFileCredentials({ profile: 'wasabi' });
// AWS.config.credentials = credentials;
var ep = new AWS.Endpoint('s3.wasabisys.com');
var s3 = new AWS.S3({
  endpoint: ep,
  accessKeyId: 'CWYP1L4VBRFJ2QD4YFIQ',
  secretAccessKey: 'glAroBhOwOIf4tzwZ14ievHhUKrGfmUwQnY4mgO6',
});
function getDateTime() {
  var today = new Date();
  var date =
    today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
  var time =
    today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
  var dateTime = date + ' ' + time;
  console.log(dateTime);
}

@Injectable()
export class ConsumerService extends AbstractKafkaConsumer {
  protected registerTopic() {
    this.addTopic('hello.topic');
    this.addTopic(HELLO_FIXED_TOPIC);
  }

  /**
   * When group id is unique for every container.
   * @param payload
   */
  @SubscribeTo('hello.topic')
  helloSubscriber(payload) {
    console.log('[KAKFA-CONSUMER] Print message after receiving');
    payload = JSON.parse(payload);
    // uploading object to s3
    console.log('[UPLOADING -FILE TO S3] Print message after receiving');
    // console.log(JSON.parse(payload['messageId']));

    const bucketName = 'talha.malikefe90e2a-ebfe-4658-90da';
    s3.createBucket({ Bucket: bucketName }, function(errBucket, dataBucket) {
      console.log('dataBucket ', dataBucket);
      if (errBucket) {
        console.log('Error creating bucket', errBucket);
      } else {
        // fileStream.on('error', function(err) {
        //   console.log('File Error', err);
        // });.
        var test = payload.body;
        console.log(payload.body.value);

        const base64Data = Buffer.from(payload.body.value.buffer);
        // console.log('==========>>>', base64Data);

        var params = {
          Bucket: bucketName,
          Key: '' + new Date().valueOf(),
          Body: base64Data,
        };
        // uploadParams.Body = fileStream;
        var path = require('path');
        // uploadParams.Key = path.basename(file);
        // call S3 to retrieve upload file to specified bucket
        s3.putObject(params, function(err, data) {
          if (err) {
            console.log('Error', err);
          }
          if (data) {
            console.log('Upload Success', data);
            getDateTime();
          }
        });
      }
    });
  }

  /**
   * When application or container scale up &
   * consumer group id is same for application
   * @param payload
   */
}
